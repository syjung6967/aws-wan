#!/usr/bin/env python3

import boto3
from botocore.exceptions import ClientError

import os
import subprocess
import multiprocessing
import functools
import binascii

# Access key ID and secret access key can be found from
# "Account identifiers" section on IAM page.
# https://console.aws.amazon.com/iam/home?#/security_credentials
#
# DO NOT EMBED YOUR KEYS IN SOURCE CODE FOR SECURITY.
AWS_ACCESS_KEY_ID = os.getenv('AWS_ACCESS_KEY_ID', None)
AWS_SECRET_ACCESS_KEY = os.getenv('AWS_SECRET_ACCESS_KEY', None)
if (AWS_ACCESS_KEY_ID is None) or (AWS_SECRET_ACCESS_KEY is None):
  print("""
    Check environment variables for AWS_ACCESS_KEY_ID and
    AWS_SECRET_ACCESS_KEY.
  """)
  exit()

def exec_cmd(cmd):
  p = subprocess.Popen(
    cmd, shell = True, stdout = subprocess.PIPE, stderr = subprocess.STDOUT
  )
  res, _ = p.communicate()
  return res

class EC2(object):
  # Available AWS EC2 regions.
  # https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html
  REGIONS = [
    #'us-east-2', # US East (Ohio)
    #'us-east-1', # US East (N. Virginia)
    #'us-west-1', # US West (N. California)
    #'us-west-2', # US West (Oregon)
    #'ap-east-1', # Asia Pacific (Hong Kong)
    #'ap-south-1', # Asia Pacific (Mumbai)
    #'ap-northeast-3', # Asia Pacific (Osaka-Local)
    'ap-northeast-2', # Asia Pacific (Seoul)
    #'ap-southeast-1', # Asia Pacific (Singapore)
    #'ap-southeast-2', # Asia Pacific (Sydney)
    'ap-northeast-1', # Asia Pacific (Tokyo)
    #'ca-central-1', # Canada (Central)
    #'eu-central-1', # Europe (Frankfurt)
    #'eu-west-1', # Europe (Ireland)
    #'eu-west-2', # Europe (London)
    #'eu-west-3', # Europe (Paris)
    #'eu-north-1', # Europe (Stockholm)
    #'me-south-1', # Middle East (Bahrain)
    #'sa-east-1', # South America (São Paulo)
  ]

  # RSA key path for each region.
  KEY_PATH = "./keys"

  # EC2 Client object cannot be "picklable" in current boto3,
  # while 'multiprocessing' modules expects the object "picklable".
  # Hence, storing the clients in advance is meaningless.
  # CLIENTS = [ ]

  def __init__(self):
    pool = multiprocessing.Pool()
    avails = pool.map(EC2.Available_region, self.REGIONS)
    self.REGIONS = [b and a for a, b in zip(self.REGIONS, avails) if b]

  # Get a low-level client representing service.
  def Client(region):
    return boto3.client(
      'ec2',
      aws_access_key_id = AWS_ACCESS_KEY_ID,
      aws_secret_access_key = AWS_SECRET_ACCESS_KEY,
      region_name = region,
    )

  def Available_region(region):
    client = EC2.Client(region)
    try:
      response = client.describe_account_attributes(
        AttributeNames = ['default-vpc'],
        DryRun = True,
      )
    except ClientError as e:
      if 'DryRunOperation' not in str(e):
        print('Error:', e)
        return False

    # Collect EC2 clients available.
    print('You have permission for region %s.' % region)
    return True

  def Generate_key_pairs(region):
    exec_cmd("mkdir -p %s" % EC2.KEY_PATH)
    exec_cmd(
      "sh -c \"echo 'y' | ssh-keygen -t rsa -m PEM -P '' -f %s/%s.pem\"" %
      (EC2.KEY_PATH, region)
    )
    exec_cmd("chmod 400 %s/%s.pem" % (EC2.KEY_PATH, region))

  def Import_key_pairs(region):
    client = EC2.Client(region)

    # Delete region key imported first, if exist.
    client.delete_key_pair(KeyName = region)

    # Read local region key file.
    key_file = open("%s/%s.pem.pub" % (EC2.KEY_PATH, region), 'r')
    res = client.import_key_pair(
      KeyName = region,
      PublicKeyMaterial=key_file.read(),
    )
    key_file.close()

    # Verify fingerprint silently.
    remote_fp = res['KeyFingerprint']
    remote_fp_bin = binascii.a2b_hex(remote_fp.replace(':', ''))
    local_fp_bin = exec_cmd(
      "sh -c \"openssl pkey -in %s/%s.pem -pubout -outform DER | openssl md5 -c -binary\"" %
      (EC2.KEY_PATH, region)
    )
    if local_fp_bin != remote_fp_bin:
      print("Fingerprint for %s unmatches." % region)
      print("local:", binascii.b2a_hex(local_fp_bin))
      print("remote:", binascii.b2a_hex(remote_fp_bin))

  # Launch the specified number of instances using an AMI for
  # which you have permissions. Do not wait for running state.
  def Run_instances(
    region,
    *,
    count,
    instance_type,
    image_name,
  ):
    image = EC2.Latest_AMI(region, image_name)
    client = EC2.Client(region)
    res = client.run_instances(
      ImageId = image['ImageId'],
      InstanceType = instance_type,
      KeyName = region,
      MinCount = count,
      MaxCount = count,
    )
    return res['Instances']

  def Terminate_instances(region):
    instances = EC2.Describe_instances(
      region,
      ['pending', 'running', 'shutting-down', 'stopping', 'stopped']
    )
    instance_ids = []
    for instance in instances:
      instance_ids.append(instance['InstanceId'])

    if instance_ids:
      print(
        "Terminate non-terminated instances for %s: %s" %
        (region, str(instance_ids))
      )

      # Terminate all instances.
      client = EC2.Client(region)
      client.terminate_instances(InstanceIds = instance_ids)

      # Wait until all instances are terminated.
      waiter = client.get_waiter('instance_terminated')
      waiter.wait(InstanceIds = instance_ids)
      print("Termination done for %s." % region)
    else:
      print("There is no instance to terminate for %s." % region)

  def Describe_instances(region, states):
    if states == 'all':
      states = [
        'pending',
        'running', 'shutting-down',
        'terminated',
        'stopping', 'stopped'
      ]
    client = EC2.Client(region)
    res = client.describe_instances(
      Filters = [{'Name': 'instance-state-name', 'Values': states}]
    )
    instances = []
    for reservation in res['Reservations']:
      for instance in reservation['Instances']:
        instances.append(instance)
    return instances

  # Find the latest Amazon Linux 2 AMI.
  # (AMI image ID differs across regions.)
  def Latest_AMI(region, image_name):
    client = EC2.Client(region)
    res = client.describe_images(
      Filters = [{'Name': 'name', 'Values': [image_name]},
                 {'Name': 'state', 'Values': ['available']}],
      Owners = ['amazon'],
    )
    res['Images'] = sorted(res['Images'], key=lambda t: t['CreationDate'])
    return res['Images'][-1]

  # Generate key pairs in local, and import public key
  # into EC2. (overwrite, if exist)
  # See also, https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html#how-to-generate-your-own-key-and-import-it-to-aws
  def import_key_pairs(self):
    pool = multiprocessing.Pool()
    pool.map(EC2.Generate_key_pairs, self.REGIONS)
    pool.map(EC2.Import_key_pairs, self.REGIONS)

  def run_instances(self,
    count,
    instance_type = 't2.micro', # free tier eligible
    image_name = 'amzn2-ami-hvm-2.0.????????.?-x86_64-gp2'
  ):
    pool = multiprocessing.Pool()
    func = functools.partial(
      EC2.Run_instances,
      count = count,
      instance_type = instance_type,
      image_name = image_name
    )
    instances = pool.map(func, self.REGIONS)
    return [item for sublist in instances for item in sublist]

  def terminate_instances(self):
    pool = multiprocessing.Pool()
    pool.map(EC2.Terminate_instances, self.REGIONS)

  def describe_instances(self, states = 'all'):
    pool = multiprocessing.Pool()
    func = functools.partial(
      EC2.Describe_instances,
      states = states
    )
    instances = pool.map(func, self.REGIONS)
    return [item for sublist in instances for item in sublist]

  """
  def Reset_default_settings(region):
    # Delete non-default VPCs. (default VPC cannot be deleted through API.)
    client = EC2.Client(region)
    res = client.describe_vpcs(
      Filters = [{'Name': 'isDefault', 'Values': ['false']}]
    )
    vpcs = res['Vpcs']
    if vpcs:
      print("Delete existing VPCs for %s. Create a new VPC." % region)
      for vpc in vpcs:
        client.delete_vpc(VpcId = vpc['VpcId'])

    res = client.create_vpc(CidrBlock = '172.31.0.0/16')
    vpc = res['Vpc']
    waiter = client.get_waiter('vpc_available')
    waiter.wait(VpcIds = [vpc['VpcId']])

  # Reset EC2 default settings.
  def reset_default_settings(self):
    pool = multiprocessing.Pool()
    pool.map(EC2.Reset_default_settings, self.REGIONS)
  """
