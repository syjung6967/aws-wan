## Deprecated due to poor parallel execution support in Boto3.
See <https://gitlab.com/syjung6967/aws-wan-scripts> for bash version.

## Prerequisites
* boto3 is installed.
* Default VPC is used to create instances.
* Security properties must be set in adavance.

## Usages
```
#!/usr/bin/env python3

import ec2
import boto3

ec2 = ec2.EC2()
print('Available regions:', ec2.REGIONS)

ec2.import_key_pairs()
print('Region keys are imported.')

ec2.run_instances(1)
print('Instances are launched.')

ec2.terminate_instances()
print('Instances are terminated.')
```

## Todos
* Share all public addresses among instances.
* Invoke a command to all instances and gather the results.
